from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import openai
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import os.path
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import pickle
import os
from google.auth.transport.requests import Request 

app = FastAPI()

openai.api_key = "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"

system_message = """
You are an AI model designed to classify prompts into four types: 'Chat', 'Mail', 'Calendar', and 'Meet'.
Please analyze the given prompt and determine its type accurately.
If the prompt is related to sending or composing an email, classify it as 'Mail'.
If the prompt is about scheduling or organizing events, classify it as 'Calendar'.
If the prompt is about initiating or joining a conversation, classify it as 'Chat'.
If the prompt is about setting up or conducting a meeting, classify it as 'Meet'.
"""

def classify_text(text):
    print(text)
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=system_message + "\n" + text,
        max_tokens=100,
        temperature=0.0
    )
    result_text = response.choices[0].text.strip()
    classification_result = result_text.split(":")[-1].strip()
    print("Classification Result:", classification_result)
    return classification_result

class MailInput(BaseModel):
    text: str
    email: str

@app.post("/classify")
def classify_text_endpoint(mail_input: MailInput):
    classification_result = classify_text(mail_input.text)
    final_res = {}
    print(mail_input.email)

    if classification_result == "Mail":
        mail_prompt = "<Compose mail based on prompt: " + mail_input.text +"and Receiver mail is :"+ mail_input.email+"and sender name is Bharani Dharan, you can only add his name in regards "">"
        print(mail_prompt)
        mail_response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=mail_prompt,
            max_tokens=100,
        )
        mail_content = mail_response.choices[0].text.strip()
        final_res["mail_content"] = mail_content

        print("Mail Content:")
        print(mail_content)
        SCOPES = ['https://mail.google.com/']

        creds = None

        if os.path.exists('token.json'):
            creds = Credentials.from_authorized_user_file('token.json', SCOPES)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=9001)
            with open('token.json', 'w') as token:
                token.write(creds.to_json())

        try:
            service = build('gmail', 'v1', credentials=creds)
           
            message = MIMEMultipart()
            message['to'] = mail_input.email
            message['from'] = "hariharasundar221199@gmail.com"
            message['subject'] = "AI-Generated Mail"

            msg = MIMEText(mail_content)
            message.attach(msg)

            raw_message = \
                base64.urlsafe_b64encode(message.as_string().encode('utf-8'))
            raw_msg = {'raw': raw_message.decode('utf-8')}
          

            
            user_id = 'me'
            message = service.users().messages().send(userId=user_id,
                body=raw_msg).execute()

            print('Message Id: {}'.format(message['id']))




        except HttpError as error:
            
            print(f'An error occurred: {error}')

            

    elif classification_result == "Chat":
        chat_prompt = "<Generate chat starting message based on prompt and the user detail is receiver mail: " + mail_input.text+"Receiver mail:"+ mail_input.email + ">"
        chat_response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=chat_prompt,
            max_tokens=50,
        )
        chat_message = chat_response.choices[0].text.strip()

        print("Chat Message:")
        print(chat_message)
        final_res["chat_message"] = chat_message

    elif classification_result == "Calendar":
        calendar_prompt = "<Extract date, Subject and time from prompt: " + mail_input.text +">"
        calendar_response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=calendar_prompt,
            max_tokens=50,
        )
        date_time = calendar_response.choices[0].text.strip()

        print("Date and Time:")
        print(date_time)
        final_res["date_time"] = date_time

    elif classification_result == "Meet":
        meet_prompt = f"<Extract meeting details from prompt to give:\nEmail:"+ mail_input.email +"Time: <provide time>\nDate: <provide date> \nSubject: <provide subject>"">"
        meet_response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=meet_prompt,
            max_tokens=50,
        )
        meet_details = meet_response.choices[0].text.strip()

        print("Meeting Details:")
        print(meet_details)
        final_res["meet_details"] = meet_details

    else:
        raise HTTPException(status_code=400, detail="Invalid classification result")

    return (final_res)