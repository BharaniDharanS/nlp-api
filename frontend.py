import gradio as gr
import requests

# Define the Gradio interface
input_text = gr.inputs.Textbox(lines=5, label="Input Text")
email_id = gr.inputs.Textbox(label="Email ID")

def process_text(input_text, email):
    # Make a request to the backend API for classification
    response = requests.post("http://localhost:8000/classify", json={"text": input_text, "email": email})
    if response.status_code == 200:
        result = response.json()
        if "mail_content" in result:
            output_text = result["mail_content"] 
        elif "chat_message" in result:
            output_text = result["chat_message"]
        elif "date_time" in result:
            output_text = result["date_time"]
        elif "meet_details" in result:
            output_text = result["meet_details"]
        else:
            output_text = "Error"
        return output_text 
    else:
        return "Error"

outputs = gr.outputs.Textbox(label="Output Text")

iface = gr.Interface(fn=process_text, inputs=[input_text, email_id], outputs=outputs)

if __name__ == "__main__":
    iface.launch()